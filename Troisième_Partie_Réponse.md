    4. Définissez le rôle des ressources suivantes : le consultant fonctionnel, le chef de projet, l'ergonome 

 • **Le consultant fonctionnel :** Son rôle est d'accompagner un client dans la mise en place d'applications liées aux métiers de l'entreprise, 
 
_source : https://www.lesjeudis.com/metiers/gestion/consultant-fonctionnel/fiche_

 • **Le chef de projet :** le chef de projet est chargé de mener un projet, de veiller à son bon déroulement, il manage son équipe de projet

 _source : https://fr.wikipedia.org/wiki/Chef_de_projet_

 • **L'ergonome :** Il a pour but de concevoir et d'ameliorer la qualité de lieux, objets ou poste de travail pour qu'ils soient confortable / facile d'utilisation et sécurisé. L'ergonome s'adapte aux besoins des utilisateurs.

 _source : http://www.onisep.fr/Ressources/Univers-Metier/Metiers/ergonome_

5. À l'aide d'un tableur, présenter un tableau de synthèse permettant de déterminer la charge de travail de chacune des ressources pour les différentes phases et leurs subdivision. Les ressources seront présentées en ligne, les phases en colonne. Pour cela, vos calculs devront faire apparaître : - la charge de travail d'une ressource pour chacune des phases de son intervention avec le détail pour les subdivisions des phases, ainsi que sa charge global de travail pour le projet - la charge de travail globale pour chacune des phases, toutes ressources cumulées. 

[Lien pour accéder au Tableau de synthèse](https://docs.google.com/spreadsheets/d/1Hc2ESmoKT0OGpAH9Ty9-dm-u86fYOAwfg_XH8oB5PJw/edit?usp=sharing)

6. Déterminer le coût global prévisionnel du projet en complétant le tableau précédent. Vous déterminerez le coût par phase et par subdivisions, le coût par ressource et le coût total du projet. 

Le coût global prévisionnel du projet est de 424 950€ (voir case : Z24)

Le coût par phase : 

    Spécifications Fonctionnelles : 55 000€ (voir C24)

        Atelier utilisateur : 10 650€ (voir C23)

        Rédaction spécifications : 32 250€ (voir D23)

        Atelier validation : 9 100€ (voir E23)

    Spécifications Techniques : 49 000€ (voir F24)

        Prise en compte : 13 750€ (voir F23)

        Rédaction du document : 32 250€ (voir G23)

    Mise en oeuvre de la plateforme technique : 10 650€ (voir H24)

    Developpement : 78 050€ (voir I24)

        Fiche Article : 6 750€ (voir I23)

        Gestion Zone Entrepôt : 8 400€ (voir J23)

        Algorithme de Routage : 11 900€ (voir K23)

        Utilisateurs et Autorisations : 4 200€ (voir L23)

        Processus entrée en stock : 5 300€ (voir M23)

        Processus sortie en stock : 5 300€ (voir N23)

        Gestion des inventaires tournants : 5 300€ (voir O23)

        Rapports : 8 950€ (voir P23)

        Gestion des alertes : 8 050€ (voir Q23)

        Edition des bons de réception : 6 950€ (voir R23)

        Edition des bons de sortie : 6 950€ (voir S23)

    Test d'Intégration : 58 000€ (voir T24)

    Recette Client : 50 750€ (voir U24)

    Formation des Utilisateurs : 29 750€ (voir V24)

        Préparation des Supports : 16 250€ (voir V23)

        Sessions de Formation : 13 500€ (voir W23)

    Mise en production, support, assistance et garantie : 93 750€ (voir X24)

Le coût total du projet est donc de 424 950€ (voir case : Z24)

Voir Tableau pour les cases citées : [Lien pour accéder au Tableau de synthèse](https://docs.google.com/spreadsheets/d/1Z7gLDomsxzLw3wgF04vjrU_70nP_jJXYFzLRcbUp-Kc/edit#gid=0)

7. Quelles observations formulez-vous sur l'importance relative de chacune des phases et sur le rôle de chacune des ressources ? De quelle marge de manoeuvre disposez-vous pour négocier avec votre client? Vous rédigerez un commentaire écrit qui sera appuyé par différents graphiques de votre choix.

  Chacune des phases dispose d'une importance relative pour le projet, elles présentent un coût déterminé par chaque ressource.
Chacune des ressources jouent un rôle important pour les différentes phases et subdivisions de ce projet. L'addition de tous les coûts
du projet présentent un coût total de 424950€ ainsi qu'une charge de travail total de 791 jours répartis entre les différentes ressources.

  Nous disposons d'une marge de manoeuvre supérieure au coût total du projet (424950€) pour pouvoir rémunérer les ressources au fur et à mesure du projet ainsi que des interêts pour couvrir d'éventuels problèmes dans la mise à bien de ce projet. 
  Il est aussi important pour notre entreprise de généré du profit pour que, sur le long terme, nous puissions mieux rémunérer nos ressources, en embaucher d'avantages, investir dans des formations et du matériel. Il est donc évident que le prix proposé à notre client soit supérieur au coût du projet.

Voir graphique si dessous:
![graphique](chart.pdf)

