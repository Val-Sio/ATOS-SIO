#Réponse aux questions de la deuxième partie du devoir "Projet_Atos"

**Deuxième Partie : La société ATOS, son identité numérique et sa e-réputation**

**1 - Définissez l'acronyme ESN. Puis présentez l'entreprise Atos : ses activités, ses résultats (chiffre d'affaires, bénéfices), ses marchés, effectif, concurrence,... Vous utiliserez différentes sources d'informations et rédigerez une note de synthèse.**

ESN : Entreprise du Service Numérique. Cet acronyme remplace l'ancien acronyme SSII (Société de Services en Ingénierie Informatique)
Une ESN est une société qui externalise son métier de génie informatique. Elle propose à ses partenaires des services numériques ainsi que son savoir-faire. De plus, on peut solliciter une ESN dans le cadre de la création d’un site internet ou d’un logiciel, la gestion d’un parc ou d’un réseau, l’étude de système informatique ainsi que la mise en place technique d’un projet client.

L'entreprise ATOS a des clients provenant de plusieurs secteurs d'activités, notamment le services financiers et d'assurances, la santé, l'industrie manufacturière, le secteur public et de la défense, les télécommunications et médias et enfin les ressources et services. L'entreprise ATOS propose à ses clients plusieurs types de solution, notamment des services d'infrastructures et de fondation, de cybersécurité, d'application, cloud, digital consulting et workspace, analytique, IA et enfin d'automatisation.
Le chiffre d'affaire de Atos s'élève à 11588 millions d'euros, son bénéfice s'élève de 2 à 7,74 euros par action.
L'effectif de cette entreprise est de 110.000 employés, elle a pour concurrent : IBM, Accenture, Capgemini, CGI, DXC, Fujitsu, T-Systems, Indra et biens d'autres encore.

Sources : https://atos.net/fr/2020/communiques-de-presse_2020_02_19/resultats-annuels-2019 , https://atos.net/fr/ , https://jobphoning.com et https://linformaticien.com

**2 - Atos est présente sur internet (https://atos.net) et sur les réseaux sociaux. Après avoir défini l'identité numérique, vous préciserez si Atos en détient une. Quel en est le principal risque ?**

L’identité numérique est l’ensemble des traces numériques qu’une personne ou une collectivité laisse sur Internet. Toutes ces informations, laissées au fil des navigations, sont collectées par les moteurs de recherche, comme Google, et sont rendues public. Une identité numérique, ou IDN, peut être constituée par : un pseudo, un nom, des images, des vidéos, des adresses IP, des favoris, des commentaires etc. Cette identité sur internet a donc une influence sur la e-réputation, sur la façon dont les internautes perçoivent une personne.
En résumé, l’identité numérique est l’image que vous renvoyez sur internet, votre image virtuelle.
L'entreprise ATOS possède donc une identité numérique, qui la représente notamment via son site internet et ses pages sur les réseaux sociaux
Le principal risque lié à l'identité numérique, c'est que l'e-réputation en soit impacté, si une information confidentielle majeure est révélée au publique, son e-reputation pourrait en prendre un coup et donc être mal vu des internautes.

Sources : https://semji.com/fr/guide/quest-ce-que-identite-numerique/ 

**3 - La société ATOS est attentive à sa e-réputation. De quoi s'agit-il ? Une entreprise concurrente souhaite s'attaquer à la réputation d'Atos sur internet. Quelles formes peuvent prendre ces attaques contre la e-réputation d'ATOS ?**


L’e-réputation peut être définie comme l’image véhiculée et / ou subie par une entreprise, un individu, une marque, un produit ou un service sur Internet et autres supports numériques. Les éléments les plus contributeurs à l'e-réputation d'une entité varient selon le domaine d'activité.
Les entreprises concurrentes pourraient attaqués leur site par le biais de DDoS afin de créer un problème de disponibilité ce qui nuira à l'e-réputation ainsi qu'au chiffre d'affaire de l'entreprise. Elles pourraient aussi exploiter une faille dans le système de sécurité afin de récupèrer des données confidentielles, ce qui créer un problème intégrité au sein de l'entreprise qui aura l'obligation d'informer ces utilisateurs. L'e-réputation sera de nouveau impacté par ces deux exemples d'attaques.

Sources : https://www.definitions-marketing.com/definition/e-reputation/